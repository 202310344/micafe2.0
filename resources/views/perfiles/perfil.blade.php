<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />


<!DOCTYPE html> 
<html lang="en">
    <head>
        <title>Perfil</title>

        <nav class="navbar navbar-expand-lg bg-body-tertiary">
          <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
              <a class="navbar-brand" href="{{ route('home') }}">MiCafe</a>
              <ul class="navbar-nav me-auto mb- mb-lg-2">
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#">Link</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" aria-current="page" href="#">Disabled</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      </head>
<hr>
<div class="container bootstrap snippet">
    <div class="row">
  		<div class="col-sm-12"><h1> {{ Auth::user()->name }}</h1></div>
    </div>
    <div class="row">
  		<div class="col-sm-12"><!--left col-->
              

      <div class="text-center">
        <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
        <h4>Insertar foto </h4>
        <input type="file" class="text-center center-block file-upload">
      </div> 
      <br>
        </div><!--/col-3-->
    	<div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#settings">Home</a></li>
              </ul>

              
              <hr>
              <form class="form" action="##" method="post" id="registrationForm">
                  <div class="form-group">
                      
                      <div class="col-xs-6">
                          <label for="first_name"><h4>First name</h4></label>
                          <input type="text" class="form-control" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any.">
                      </div>
                  </div>
                  <div class="form-group">
                      
                      <div class="col-xs-6">
                        <label for="last_name"><h4>Last name</h4></label>
                          <input type="text" class="form-control" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any.">
                      </div>
                  </div>
                
                  <div class="form-group">
                      
                      <div class="col-xs-6">
                          <label for="email"><h4>Email</h4></label>
                          <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email.">
                      </div>
                  </div>

                  <div class="form-group">
                      
                      <div class="col-xs-6">
                          <label for="password"><h4>Password</h4></label>
                          <input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
                      </div>
                  </div>
                  <div class="form-group">
                      
                      <div class="col-xs-6">
                        <label for="password2"><h4>Verify</h4></label>
                          <input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
                      </div>
                  </div>
                  <div class="form-group">
                       <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            @push('scripts')
                                <script>
                                    @if(session('perfilactualizado'))
                                        window.location = '{{ route("menu.index") }}' + '#producto-{{ session("perfil actualizado")->id }}';
                                    @endif
                                </script>
                            @endpush        
                               <!--<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>-->
                        </div>
                  </div>
              </form>
          </div>
              <hr>
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
</body>
</html> 
                                      <!-- Footer-->
 <footer class="py-5 bg-dark">
    <div class="container"><p class="m-0 text-center text-white">Copyright &copy; MiCafe 2023</p></div>
</footer>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
  <!-- JavaScript -->
               