@extends('layouts.app')

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title">usuarios registrados </h4>
            <p class="card-category">asignacion de roles a usauarios </p>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12 text-right">
                               
              </div>
            </div>
            <div class="table-responsive">
              <table class="table ">
                <thead class="text-primary">
                    <h1>Lista de usuarios</h1>
                    <ul>
                        @foreach ($users as $user)
                            <li>{{ $user->name }}</li>
                        @endforeach
                    </ul>
                </thead>
                <div class="row">
                    <label for="roles" class="col-sm-2 col-form-label">Roles</label>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <div class="tab-content">
                                <div class="tab-pane active">
                                    <table class="table">
                                        <tbody>
                                            @foreach ($roles as $id => $role)
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" name="roles[]"
                                                                value="{{ $id }}"
                                                            >
                                                            <span class="form-check-sign">
                                                                <span class="check"></span>
                                                            </span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{ $role }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                   
                    </td>
                </tbody>
              </table>
              
          <!--Footer-->
          
            <div class="card-footer ml-auto mr-auto">
                <form method="POST" action="{{ route('asignar_rol') }}">
                    @csrf
                    <!-- Otros campos del formulario si los hubiera -->
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
              </div>
          
          <!--End footer-->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
