<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th class="text-center">Precio</th>
                        <th class="text-center">Total</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($carrito as $index => $item)
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> 
                                <img class="media-object" src="https://icons.iconarchive.com/icons/ionic/ionicons/256/fast-food-outline-icon.png" style="width: 72px; height: 72px;"> 
                            </a>
                            <div class="media-body">
                                <div class="producto">
                                <h4>{{ $item['producto']->nombre }}</h4><h4 class="media-heading"></h4>
                                </div>
                            </div>
                        </div>
                        </td>
                        <td class="col-sm-1 col-md-1" style="text-align: center">
                        <p>{{ $item['cantidad'] }}</p>
                        </td>
                        <td class="col-sm-1 col-md-1 text-center"><p>${{ $item['producto']->precio }}</p></td>
                        <td class="col-sm-1 col-md-1 text-center"><strong>${{ $item['producto']->precio * $item['cantidad'] }}</strong></td>
                        <td class="col-sm-1 col-md-1">
                        <form id="remove-form-{{ $index }}" action="{{ route('carrito.remove', ['index' => $index]) }}" method="POST" style="display: none;">                                
                        @csrf
                            </form>
                            <button type="button" class="btn btn-danger remove-btn" data-index="{{ $index }}">
                                <span class="glyphicon glyphicon-remove"></span> Remove
                            </button>
                    </td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong>${{ $total }}</strong></h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                        <a href="{{ route('home') }}" class="btn btn-default">
                        <span class="glyphicon glyphicon-shopping-cart"></span> Seguir comprando
                        </a>
                        </td>
                        <td>
                            <form action="{{ route('pagar') }}" method="POST">
                                <!-- Resto del código -->
                                @csrf
                                <td>
                                    <button type="submit" class="btn btn-success" name="pagar">
                                        Pagar <span class="glyphicon glyphicon-play"></span>
                                    </button>
                                </td>
                            </form>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<form id="remove-form" action="{{ route('carrito.remove', ['index' => '__index__']) }}" method="POST" style="display: none;">
    @csrf
    <input type="hidden" name="index" id="remove-index">
</form>
<script>
    $(document).ready(function() {
        $('.remove-btn').click(function() {
            var index = $(this).data('index');
            $('#remove-index').val(index);
            $('#remove-form').attr('action', $('#remove-form').attr('action').replace('__index__', index));
            $('#remove-form').submit();
        });
    });
</script>
<!-- Resto del contenido de la vista -->

