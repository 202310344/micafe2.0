<!-- resources/views/productos/create.blade.php -->
@extends('layouts.app') 
<link rel="stylesheet" href="style.css">

@section('content')
<div class="container text-center">
        <div class="row align-items-start">
            <div class="col-5">
                <h1>Crear Producto</h1>
            </div>
        </div>
    <form action="{{ route('productos.store') }}" method="POST">
        @csrf

        <div class="col-1 col-sm-10">
                <div class="form-group">
                    <div class="col-1">
                        <br>
                         <label for="nombre">Nombre:</label>
                    </div>
                </div>
            <input type="text" name="nombre" class="form-control">
        </div>

        <div class="col-1 col-sm-10">
                <div class="form-group">
                    <div class="col-1">
                        <br>
                            <label for="detail">Detalle:</label>
                    </div>
                </div>
            <input type="text" name="detail" class="form-control">
        </div>

        <div class="col-1 col-sm-10">
                <div class="form-group">
                    <div class="col-1">
                        <br>
                            <label for="precio">Precio:</label>
                    </div>
                </div>
            <input type="number" name="precio" class="form-control">
        </div>

        <div class="col-1 col-sm-10">
                <div class="form-group">
                    <div class="col-1">
                        <br>
                            <label for="formFileSm" class="form-label">Imagen</label>
                        </div>
                    </div>              
                    <input class="form-control form-control-sm" id="formFileSm" type="file">
                </div>
            <div>

            <br>    

        <button type="submit" class="btn btn-primary">Guardar</button>

        <a href="{{ route('productos.index') }}" class="btn btn-primary">Volver</a>
    </form>
@endsection
@push('scripts')
    <script>
        @if(session('productoCreado'))
            window.location = '{{ route("productos.index") }}' + '#producto-{{ session("productoCreado")->id }}';
        @endif
    </script>
@endpush    