
@foreach ($pedidos as $pedido)
    @if (is_object($pedido) && isset($pedido->producto))
            <h4>Producto: {{ $pedido->producto->nombre }}</h4>
            <p>Cantidad: {{ $pedido->cantidad }}</p>
            <p>Precio: ${{ $pedido->producto->precio }}</p>
            <p>Total: ${{ $pedido->producto->precio * $pedido->cantidad }}</p>
    @endif
@endforeach
