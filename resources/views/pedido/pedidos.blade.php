@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th class="text-center">Precio</th>
                            <th class="text-center">Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pedidos as $pedido)
                            <tr>
                                <td class="col-sm-8 col-md-6">
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="#"> 
                                            <img class="media-object" src="https://icons.iconarchive.com/icons/ionic/ionicons/256/fast-food-outline-icon.png" style="width: 72px; height: 72px;"> 
                                        </a>
                                        <div class="media-body">
                                            <div class="producto">
                                                <!-- Mostrar nombre del producto del pedido -->
                                                <h4>{{ $pedido->producto->nombre }}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="col-sm-1 col-md-1" style="text-align: center">
                                    <!-- Mostrar cantidad del pedido -->
                                    <p>{{ $pedido->cantidad }}</p>
                                </td>
                                <td class="col-sm-1 col-md-1 text-center">
                                    <!-- Mostrar precio del producto del pedido -->
                                    <p>${{ $pedido->producto->precio }}</p>
                                </td>
                                <td class="col-sm-1 col-md-1 text-center">
                                    <!-- Mostrar total del pedido -->
                                    <strong>${{ $pedido->producto->precio * $pedido->cantidad }}</strong>
                                </td>
                                <td class="col-sm-1 col-md-1">
                                    <form id="remove-form-{{ $pedido->id }}" action="{{ route('carrito.remove', ['index' => $pedido->id]) }}" method="POST" style="display: none;">                                
                                        @csrf
                                    </form>
                                    <button type="button" class="btn btn-danger remove-btn" data-index="{{ $pedido->id }}">
                                        <span class="glyphicon glyphicon-remove"></span> Remove
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection



