<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\CarritoController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\userController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Pedido;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('products', ProductController::class);

Route::resource('productos', ProductoController::class);

Route::resource('perfils', ProductoController::class);



Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/perfil', function () {
    return view('perfiles.perfil');
})->name('perfil');

Route::get('/carrito', [CarritoController::class, 'mostrarCarrito'])->name('carrito');

Route::get('/pagar', [CarritoController::class, 'pagar'])->name('pagar');

Route::post('/carrito/agregar', [CarritoController::class, 'agregarAlCarrito'])->name('agregarAlCarrito');

Route::get('/carrito/eliminar/{productoId}', [CarritoController::class, 'eliminarDelCarrito'])->name('eliminarDelCarrito');

Route::post('/carrito/remove/{index}', [CarritoController::class, 'remove'])->name('carrito.remove');

// routes/web.php

Route::get('/pedidos', function () {
    $pedidos = App\Models\Pedido::all(); // Obtén los pedidos desde el modelo Pedido (ajusta esto según tus necesidades)
    return view('pedido.pedidos', compact('pedidos'));
})->name('pedido.pedidos');





Route::get('/pedidos', [PedidoController::class, 'index'])->name('pedidos.index');

Route::post('/pedidos', [PedidoController::class, 'guardar'])->name('pedidos.guardar');



Route::resource('permission', App\Http\Controllers\permissionController::class);
Route::resource('roles', App\Http\Controllers\RoleController::class);

Route::get('/usuarios', [userController::class, 'index'])->name('usarios.index');

Route::post('/asignar-rol', [App\Http\Controllers\AsignarRolController::class, 'asignarRol'])->name('asignar_rol');
