<?php

// app/Http/Controllers/PedidoController.php

namespace App\Http\Controllers;

use App\Models\Pedido;
use Illuminate\Http\Request;


class PedidoController extends Controller
{


    public function index()
{
    // Lógica para obtener los pedidos
    $pedidos = Pedido::all();

    // Verifica que los pedidos sean válidos y tengan una propiedad 'producto'
    


    // Pasa los pedidos a la vista
    return view('pedido.pedidos', compact('pedidos'));
}


public function realizarPedido(Request $request)
{
    // Obtén los detalles del pedido del formulario de envío
    $productoId = $request->input('producto_id');
    $cantidad = $request->input('cantidad');
    // Otros campos relevantes del pedido

    // Crea una nueva instancia del modelo Pedido
    $pedido = new Pedido();
    $pedido->producto_id = $productoId;
    $pedido->cantidad = $cantidad;
    // Asigna otros valores relevantes al pedido

    // Guarda el pedido en la base de datos
    $pedido->save();

    // Redirecciona o realiza otras acciones necesarias
}


public function mostrarPedidos()
{
    $pedidos = Pedido::all(); // Obtén todos los pedidos realizados por el usuario

    return view('carrito')->with('pedidos', $pedidos);
}

public function mostrarPedido()
{
    $productos = Producto::all(); // Obtén los productos desde tu modelo o cualquier otra fuente de datos

    return view('vista_pedido', ['productos' => $productos]);
}


public function pagar(Request $request)
{
    $rules = [
        'nombre' => 'required',
        'email' => 'required|email',
        'direccion' => 'required',
        // Agrega aquí más reglas de validación según tus necesidades
    ];

    $messages = [
        'nombre.required' => 'El campo Nombre es obligatorio.',
        'email.required' => 'El campo Email es obligatorio.',
        'email.email' => 'El campo Email debe ser una dirección de correo electrónico válida.',
        'direccion.required' => 'El campo Dirección es obligatorio.',
        // Agrega aquí más mensajes de error personalizados si lo deseas
    ];
    $validatedData = $request->validate($rules, $messages);
     // Procesa los datos después de la validación
     $nombre = $validatedData['nombre'];
     $email = $validatedData['email'];
     $direccion = $validatedData['direccion'];
 
     // Ejemplo de procesamiento: guardar los datos en la base de datos
     $pedido = new Pedido();
     $pedido->nombre = $nombre;
     $pedido->email = $email;
     $pedido->direccion = $direccion;
     $pedido->save();
 
     // Otro procesamiento: enviar un correo electrónico de confirmación
     Mail::to($email)->send(new ConfirmacionPedido($pedido));
 
     // Redirigir a una página de éxito o mostrar un mensaje de éxito
     return redirect()->route('pedido.success')->with('success', '¡El pedido se ha procesado con éxito!');

}
}