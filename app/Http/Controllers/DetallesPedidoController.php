<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pedido;

class DetallesPedidoController extends Controller
{
    public function mostrarDetalles(Request $request)
    {
        // Lógica para obtener los pedidos y asignarlos a la variable $pedidos
        $pedidos = Pedido::where('estado', 'confirmado')->get(); // Ejemplo: Obtener los pedidos confirmados

        return view('detalles_pedidos', compact('pedidos'));
    }
}
