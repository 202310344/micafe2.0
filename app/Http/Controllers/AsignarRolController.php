<?php

namespace App\Http\Controllers;

use App\Models\User;

use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;

class AsignarRolController extends Controller
{
    public function asignarRol(Request $request)
    {
        
        // Validar los datos recibidos en la solicitud
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'role_id' => 'required|exists:roles,id',
        ]);

        // Obtener el usuario y el rol
        $user = User::findOrFail($request->user_id);
        $role = Role::findOrFail($request->role_id);

        // Asignar el rol al usuario
        $user->roles()->attach($role);


        // Redireccionar a otra página después de guardar los datos
        return redirect()->route('menu.home');
    }
}
